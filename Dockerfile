FROM nginx:stable

COPY nginx.conf /etc/nginx/nginx.conf

COPY default.conf /etc/nginx/conf.d/default.conf

COPY index-lacapsule.html /usr/share/nginx/html/index.html

EXPOSE 81

CMD ["nginx", "-g", "daemon off;"]
